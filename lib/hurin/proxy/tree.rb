module Hurin
  module Proxy
    class Tree < Hurin::Entity::GitObject::Tree

      attr_accessor :sha

      protected
      attr_accessor :repo, :tree

      public
      def initialize repo, sha
        @repo, @sha, @tree = repo, sha, false
      end

      def set_sha sha
        self.load
        @tree.sha = sha
      end

      def add_node node
        raise ArgumentError unless node.is_a? Hurin::Entity::GitObject::Node::BaseNode

        self.load
        @tree.add_node node
      end

      def get_nodes
        self.load
        return @tree.get_nodes
      end

      def get_node_named name
        self.load
        return @tree.get_node_named name
      end

      def has_node_named? name
        self.load
        return @tree.has_node_named? name
      end

      def get_iterator
        self.load
        return @tree.get_iterator
      end

      protected
      def load
        unless @tree then
          @tree = @repo.get_object_by_sha sha
        end
      end
    end
  end
end
