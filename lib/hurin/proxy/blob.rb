module Hurin
  module Proxy
    class Blob < Hurin::Entity::GitObject::Blob

      attr_accessor :sha

      protected
      attr_accessor :repo, :blob

      public
      def initialize repo, sha
        @repo, @sha, @blob = repo, sha, false
      end

      def set_sha sha
        self.load
        @blob.sha = sha
      end

      def set_contents content
        self.load
        @blob.content = content
      end

      def get_content
        self.load
        return @blob.content
      end

      protected
      def load
        unless @blob then
          @blob = @repo.get_object_by_sha sha
        end
      end
    end
  end
end
