module Hurin
  module Proxy
    class Commit < Hurin::Entity::GitObject::Commit

      attr_accessor :sha

      protected
      attr_accessor :commit, :repo

      public
      def initialize repo, sha
        @repo, @sha, @commit = repo, sha, false
      end

      def set_sha sha
        self.load
        @commit.sha = sha
      end

      def set_message message
        self.load
        @commit.message = message
      end

      def get_message
        self.load
        return @commit.message
      end

      def set_tree tree
        raise ArgumentError unless tree.is_a? Hurin::Entity::GitObject::Tree
        self.load
        @commit.tree = tree
      end

      def get_tree
        self.load
        return @commit.tree
      end

      def add_parent parent
        raise ArgumentError unless parent.is_a? Hurin::Entity::GitObject::Commit

        self.load
        @commit.add_parent parent
      end

      def get_parents
        self.load
        return @commit.parents
      end

      def set_author author
        raise ArgumentError unless author.is_a? Hurin::Entity::GitObject::User
        self.load
        @commit.author = author
      end

      def get_author
        self.load
        return @commit.author
      end

      def set_author_time time
        self.load
        @commit.author_time = time
      end

      def get_author_time
        self.load
        return @commit.author_time
      end

      def set_committer user
        raise ArgumentError unless user.is_a? Hurin::Entity::GitObject::User
        self.load
        @commit.committer = user
      end

      def get_committer
        self.load
        return @commit.committer
      end

      def set_commit_time time
        self.load
        @commit.commit_time = time
      end

      def get_commit_time
        self.load
        return @commit.commit_time
      end

      protected
      def load
        unless @commit then
          @commit = @repo.get_object_by_sha @sha
        end
      end
    end
  end
end
