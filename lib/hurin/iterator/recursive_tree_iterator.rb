module Hurin
  module Iterator
    class RecursiveTreeIterator
      attr_accessor :nodes

      def initialize nodes
        @nodes = nodes
      end
    end
  end
end
