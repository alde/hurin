module Hurin
  module Transport
    class BaseTransport

      def initialize
        name = self.class.to_s.split('::').last
        raise "Can't be instantiated" if name === 'BaseTransport'
      end

      def resolve_treeish
      end

      def set_branch branch, sha
      end
    end
  end
end
