module Hurin
  module Entity
    module GitObject
      class Tree
        attr_accessor :sha, :nodes

        def initialize
          @nodes = []
        end

        def get_iterator
          return Hurin::Iterator::RecursiveTreeIterator.new(@nodes)
        end

        def add_node node
          raise "Expected BaseNode but got #{node.class}" unless node.is_a? Hurin::Entity::GitObject::Node::BaseNode

          @nodes << node
          @nodes.sort! { |a, b| a.name <=> b.name }
        end

        def get_node_named name
          @nodes.each { |n| return n if n.name == name }
          return nil
        end

        def has_node_named? name
          @nodes.each { |n| return true if n.name == name }
          return false
        end
      end
    end
  end
end
