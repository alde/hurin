module Hurin
  module Entity
    module GitObject
      module Node
        class BaseNode
          attr_accessor :mode, :name

          def octal_mode
            return sprintf("%o", @mode)
          end

          def ===(other_node)
            if @mode === other_node.mode and @name === other_node.name then
              our_related = get_related_object
              their_related = other_node.get_related_object
              if our_related === their_related then
                return true
              else
                if not our_related.sha.empty? and not their_related.sha.empty? then
                  return our_related.sha === their_related.sha
                end
              end
            end
            return false
          end

          def get_related_object
          end
        end
      end
    end
  end
end
