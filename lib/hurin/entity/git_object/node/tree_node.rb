module Hurin
  module Entity
    module GitObject
      module Node
        class TreeNode < Hurin::Entity::GitObject::Node::BaseNode
          attr_accessor :tree

          def initialize
            @mode = 040000
          end

          def get_related_object
            return @tree
          end
        end
      end
    end
  end
end
