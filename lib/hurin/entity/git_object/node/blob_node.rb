module Hurin
  module Entity
    module GitObject
      module Node
        class BlobNode < Hurin::Entity::GitObject::Node::BaseNode
          attr_accessor :blob

          def get_related_object
            return @blob
          end
        end
      end
    end
  end
end
