module Hurin
  module Entity
    module GitObject
      class User
        attr_accessor :name, :email

        def initialize name, email
          @name, @email = name, email
        end

        def == other
          return false unless other.is_a? Hurin::Entity::GitObject::User
          return false if @name != other.name
          return false if @email != other.email
          return true
        end
      end
    end
  end
end
