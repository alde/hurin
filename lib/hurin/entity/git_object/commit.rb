module Hurin
  module Entity
    module GitObject
      class Commit
        attr_accessor :sha, :tree, :parents, :author, :committer, :message
        attr_accessor :author_time, :commit_time

        def initialize
          @parents = []
        end

        def add_parent parent
          raise "Parent must be a commit" unless parent.is_a?(Hurin::Entity::GitObject::Commit)
          @parents << parent
        end
      end
    end
  end
end
