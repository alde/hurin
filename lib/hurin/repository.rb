module Hurin

  class Repository
    attr_accessor :populators, :extractors, :index_populator, :index_extractor
    attr_accessor :index, :unflushed, :branch_moves, :branch_removes
    attr_accessor :transport

    def initialize
      @populators = {}
      @extractors = {}
      @branch_removes = []
      @branch_moves = {}
      @unflushed = {}
    end

    def set_populator type, populator
      unless @populators.has_key? type
        @populators[type] = {}
      end
      @populators[type][:populator] = populator
    end

    def set_extractor type, extractor
      unless @extractors.has_key? type
        @extractors[type] = {}
      end
      @extractors[type][:extractor] = extractor
    end

    def get_populator_for_type type
      @populators[type][:populator]
    end

    def get_extractor_for_type type
      @extractors[type][:extractor]
    end

    def get_object_type object
      return :blob if object.is_a? Hurin::Entity::GitObject::Blob
      return :tree if object.is_a? Hurin::Entity::GitObject::Tree
      return :commit if object.is_a? Hurin::Entity::GitObject::Commit
    end

    def index
      if @index.nil? then
        if not @transport.nil? and @transport.has_index_data then
          @index = @index_populator.populate @transport.index_data
        else
          @index = Hurin::Entity::Index.new
        end
      end

      return @index
    end

    def flush_index
      unless @index.nil? then
        @transport.put_index_data(@index_extractor.extract(index))
      end
    end

    def flush
      @unflushed.each do |sha, raw|
        @transport.put_raw_object raw
      end

      flush_index()

      @branch_moves.each do |branch, commit|
        @transport.set_branch(branch, commit.sha)
      end

      @branch_removes.each do |branch, commit|
        @transport.remove_branch(branch)
      end
    end

    def set_branch branch, commit
      # Delete the branch from removal queue if it is there
      @branch_removes.delete_if { |key| key == branch }

      @branch_moves[branch] = commit
    end

    def remove_branch branch
      @branch_removes << branch
    end

    def rename_branch from, to
      commit = Hurin::Proxy::Commit.new(
        self, @transport.resolve_treeish(from)
      )
      set_branch(to, commit)
      remove_branch(from)
    end

    def has_tag? tag
      return !@transport.resolve_tag(tag).nil?
    end

    def has_object? treeish
      return !@transport.resolve_treeish(treeish).nil?
    end

    def get_object treeish
      sha = @transport.resolve_treeish(treeish)
      raise "Could not find an object with identifier #{treeish.inspect}" if sha.nil?
      return get_object_by_sha(sha)
    end

    def get_object_by_sha sha
      raw = @transport.fetch_raw_object sha
      raise "Could not fetch object with identifier #{sha.inspect}" if raw.nil?

      populator = get_populator_for_type raw.type
      raise "No populator found for type #{raw.type}" if populator.nil?

      return populator.populate(raw)
    end

    def extract_git_object object
      if object.sha.nil? then
        return
      end

      if object.is_a? Hurin::Entity::GitObject::Blob then
        do_extraction(object)
      elsif object.is_a? Hurin::Entity::GitObject::Tree then
        object.nodes.each do |node|
          extract_git_object(node.related_object)
        end
        do_extraction object
      elsif  object.is_a? Hurin::Entity::GitObject::Commit then
        extract_git_object(object.tree)
        object.parents.each do |parent|
          extract_git_object(parent)
        end
        do_extraction(object)
      end
    end

    protected
    def do_extraction object
      type = get_object_type object
      extractor = get_extractor_for_type type
      raw = extractor.extract object
      object.sha = raw.sha
      @unflushed[raw.sha] = raw
    end
  end
end
