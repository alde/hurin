module Hurin

  class Configurator

    def defaultConfig repository
      repository.set_populator :commit, Hurin::Populator::CommitPopulator.new(repository)
      repository.set_extractor :commit, Hurin::Extractor::CommitExtractor.new(repository)

      repository.set_populator :tree, Hurin::Populator::TreePopulator.new(repository)
      repository.set_extractor :tree, Hurin::Extractor::TreeExtractor.new(repository)
      
      repository.set_populator :blob, Hurin::Populator::BlobPopulator.new(repository)
      repository.set_extractor :blob, Hurin::Extractor::BlobExtractor.new(repository)

      repository.index_populator = Hurin::Populator::IndexPopulator.new(repository)
      repository.index_extractor = Hurin::Extractor::IndexExtractor.new(repository)
    end
  end
end