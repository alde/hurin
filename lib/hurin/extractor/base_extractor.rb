module Hurin
  module Extractor
    class BaseExtractor
      def initialize
        name = self.class.to_s.split('::').last
        raise "Can't be instantiated" if name === 'BaseExtractor'
      end

      def extract
      end
    end
  end
end
