module Hurin
  module Populator
    class BasePopulator
      def initialize
        name = self.class.to_s.split('::').last
        raise "Can't be instantiated" if name === 'BasePopulator'
      end

      def populate raw_object
      end
    end
  end
end
