module Hurin
  module Populator
    class BlobPopulator < BasePopulator
      def populate raw_object
        blob = Hurin::Entity::GitObject::Blob.new
        blob.sha = raw_object.sha
        blob.content = raw_object.data

        return blob
      end
    end
  end
end
