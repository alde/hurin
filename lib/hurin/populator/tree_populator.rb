module Hurin
  module Populator
    class TreePopulator < BasePopulator

      protected
      attr_accessor :repo

      public
      ##
      # Initialize the TreePopulator
      def initialize repo
        @repo = repo
      end

      ##
      # Populate a tree from a raw object
      #
      # === returns
      # Hurin::Entity::GitObject::Tree - A Tree object
      def populate raw_object
        tree = Hurin::Entity::GitObject::Tree.new
        tree.sha = raw_object.sha

        reader = StringIO.new raw_object.data
        while not reader.eof?
          mode, name = read_mode(reader), read_name(reader)
          sha = reader.read(20).unpack("H*")[0]

          node = create_node sha, ((mode & 040000) != 0)

          node.mode, node.name = mode, name

          tree.add_node node
        end

        return tree
      end

      private
      ##
      # Read mode from stream
      #
      # === returns
      # ::Integer - mode in base 8
      def read_mode reader
        mode_string = ''
        catch(:found) do
          while char = reader.read(1) do
            mode_string += char if (numeric? char)
            throw :found if char == ' '
            raise "Expected number or space, got <#{char}> at position #{reader.pos}" if not numeric? char
          end
        end

        return mode_string.to_i(8)
      end

      ##
      # Read name from stream
      #
      # === returns
      # ::String - name
      def read_name reader
        name = ''
        catch(:found) do
          while char = reader.read(1) do
            name += char unless char == "\0"
            throw :found if char == "\0"
          end
        end

        return name
      end

      ##
      # Create a node from a hash.
      # If is_tree is true, a TreeNode will be created. Otherwise, a BlobNode
      #
      # === returns
      # Hurin::Entity::GitObject::Node::BaseNode - Extension of BaseNode
      def create_node sha, is_tree
        node = nil
        if is_tree then
          node = Hurin::Entity::GitObject::Node::TreeNode.new
          node.tree = Hurin::Proxy::Tree.new @repo, sha
        else
          node = Hurin::Entity::GitObject::Node::BlobNode.new
          node.blob = Hurin::Proxy::Blob.new @repo, sha
        end

        return node
      end

      ##
      # Check if an object is numeric.
      #
      # === returns
      # ::Boolean - true if supplied object is numeric, otherwise false
      def numeric? object
        true if Float(object) rescue false
      end
    end
  end
end
