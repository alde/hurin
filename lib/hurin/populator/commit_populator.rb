module Hurin
  module Populator
    class CommitPopulator < BasePopulator
      require "scanf"

      protected
      attr_accessor :repo

      public
      def initialize repo
        @repo = repo
      end

      def populate raw_object
        commit = Hurin::Entity::GitObject::Commit.new
        commit.sha = raw_object.sha
        meta, message = raw_object.data.split("\n\n")

        commit.message = message
        meta.split("\n").each do |meta_line|
          attribute = meta_line.scanf("%s")[0]
          attr_value = meta_line[(attribute.length+1)..-1]

          case attribute.to_sym
          when :tree
            commit.tree = Hurin::Proxy::Tree.new @repo, attr_value
          when :parent
            commit.add_parent Hurin::Proxy::Commit.new @repo, attr_value
          when :author
            commit.author, commit.author_time = get_user attr_value
          when :committer
            commit.committer, commit.commit_time = get_user attr_value
          end
        end

        return commit
      end

      private
      def get_user string
        matches = string.scan(/(.*?) <(.*?)> ([0-9]*)( (.+))?/)[0]
        return [
          Hurin::Entity::GitObject::User.new(matches[0], matches[1]),
          Time.at(matches[2].to_i)
        ]
      end
    end
  end
end
