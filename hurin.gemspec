$:.push File.expand_path("../lib", __FILE__)
require "rake"
require 'hurin/version'

Gem::Specification.new do |s|
    s.platform      = Gem::Platform::RUBY
    s.authors       = ['Rickard Dybeck']
    s.email         = ['r.dybeck@gmail.com']
    s.homepage      = "http://alde.nu"
    s.summary       = "Hurin is a native GIT implementation in Ruby"
    s.name          = "hurin"
    s.version       = Hurin::VERSION
    s.date          = "2013-03-01"
    s.files         = FileList['lib/**/*.rb']
    s.require_paths = ["lib"]
end
