require 'testhelper'

module Hurin
  describe Repository do
    include Ramcrest::Is

    before do
      @repo = Repository.new
    end

    it "can check if an object should exist" do
      transport = StubTransport.new

      transport.expects(:resolve_treeish).with('foo').returns('deadbeefcafe').once()
      transport.expects(:resolve_treeish).with('bar').returns(nil).once
      @repo.transport = transport

      assert_that @repo.has_object?('foo'), is(true)
      assert_that @repo.has_object?('bar'), is(false)
    end

    it "can set and get a populator" do
      populator = StubPopulator.new
      @repo.set_populator 'foo', populator
      assert_that @repo.get_populator_for_type('foo'), is(populator)
    end

    it "can set and get an extractor" do
      extractor = StubExtractor.new
      @repo.set_extractor 'foo', extractor
      assert_that @repo.get_extractor_for_type('foo'), is(extractor)
    end

    it "can check if a tag exists" do
      transport = StubTransport.new

      transport.expects(:resolve_tag).with('foo').returns('deadbeefcafe').once()
      transport.expects(:resolve_tag).with('bar').returns(nil).once()

      @repo.transport = transport

      assert_that @repo.has_tag?('foo'), is(true)
      assert_that @repo.has_tag?('bar'), is(false)
    end

    it "can identify different types of GitObjects" do
      assert_that(
        @repo.get_object_type(Hurin::Entity::GitObject::Blob.new),
        is(:blob)
      )
      assert_that(
        @repo.get_object_type(Hurin::Entity::GitObject::Tree.new),
        is(:tree)
      )
      assert_that(
        @repo.get_object_type(Hurin::Entity::GitObject::Commit.new),
        is(:commit)
      )
      assert_that(
        @repo.get_object_type(Hurin::Entity::GitObject::Node::TreeNode.new),
        is(nil)
      )
    end

    it "can remove a branch" do
      transport = StubTransport.new
      transport.expects(:remove_branch).with('foo').once()

      @repo.transport = transport
      @repo.remove_branch "foo"
      @repo.flush
    end

    it "can set a branch after it has been removed" do
      transport = StubTransport.new
      transport.expects(:set_branch).with('foo', 'f00bar').once()

      commit = stub('commit')
      commit.stubs(:sha).returns('f00bar')

      @repo.transport = transport
      @repo.remove_branch "foo"
      @repo.set_branch "foo", commit
      @repo.flush
    end

    it "can rename a branch" do
      transport = StubTransport.new
      transport.expects(:resolve_treeish).with('foo').returns('f00bar')
      transport.expects(:remove_branch).with('foo').once()
      transport.expects(:set_branch).with('bar', 'f00bar').once()

      @repo.transport = transport
      @repo.rename_branch 'foo', 'bar'
      @repo.flush
    end

    it "will return cached index if it exists" do
      index = stub('index')
      @repo.index = index
      assert_that @repo.index, is(index)
    end

    it "will create a new index if no cache exists" do
      assert_that @repo.index.is_a?(Hurin::Entity::Index), is(true)
    end
    it "will populate index from transport if possible" do
      index = stub('index')
      transport = StubTransport.new
      transport.expects(:index_data).returns(index)
      transport.expects(:has_index_data).returns(true)

      index_populator = stub('index_populator')
      index_populator.stubs(:populate).returns(index)
      @repo.transport = transport
      @repo.index_populator = index_populator

      assert_that @repo.index, is(index)
    end

    it "can flush index" do
      index = stub('index')

      @repo.index = index

      extractor = StubExtractor.new
      extractor.expects(:extract).with(index).returns('foo')
      transport = StubTransport.new
      transport.expects(:put_index_data).with('foo').once

      @repo.index_extractor = extractor
      @repo.transport = transport

      @repo.flush_index
    end
  end

  class StubTransport < Transport::BaseTransport
  end

  class StubPopulator < Populator::BasePopulator
  end

  class StubExtractor < Extractor::BaseExtractor
  end
end
