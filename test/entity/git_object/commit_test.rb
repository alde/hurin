require 'testhelper'

module Hurin
  module Entity
    module GitObject

      describe Commit do
        include Ramcrest::EqualTo
        include Ramcrest::HasSize
        include Ramcrest::Includes

        before do
          @commit = Commit.new
        end

        it "can hold a sha hash" do
          @commit.sha = "deadbeef"
          assert_that @commit.sha, equal_to("deadbeef")
        end

        it "can have a message" do
          @commit.message = "message"
          assert_that @commit.message, equal_to("message")
        end

        it "can set the tree" do
          mock_tree = Tree.new
          @commit.tree = mock_tree
          assert_that @commit.tree, equal_to(mock_tree)
        end

        it "can set the parent" do
          assert_that @commit.parents, has_size(0)
          parent = Commit.new
          @commit.add_parent parent

          assert_that @commit.parents, includes(parent)
          assert_that @commit.parents, has_size(1)
        end

        it "can have author" do
          author = User.new 'foo', 'bar@baz.se'
          @commit.author = author
          assert_that @commit.author, equal_to(author)
        end

        it "can have committer" do
          committer = User.new 'foo', 'bar@baz.se'
          @commit.committer = committer
          assert_that @commit.committer, equal_to(committer)
        end

        it "can keep track of commit time" do
          commit_time = Time.now
          @commit.commit_time = commit_time
          assert_that @commit.commit_time, equal_to(commit_time)
        end

        it "can keep track of author time" do
          author_time = Time.now
          @commit.author_time = author_time
          assert_that @commit.author_time, equal_to(author_time)
        end
      end
    end
  end
end
