require 'testhelper'

module Hurin
  module Entity
    module GitObject
      describe Tree do
        include Ramcrest::EqualTo

        attr_accessor :tree, :node_mock

        before do
          @node_mock = Hurin::Entity::GitObject::Node::BlobNode.new
          @node_mock.stubs(:name).returns('foobar')
          @tree = Hurin::Entity::GitObject::Tree.new
          @tree.add_node @node_mock
        end

        it "can hold a sha hash" do
          @tree.sha = "deadbeef"
          assert_that "deadbeef", equal_to(@tree.sha)
        end

        it "can get all nodes" do
          assert_that [@node_mock], equal_to(@tree.nodes)
        end

        it "can check if it has a named node" do
          assert_that true, equal_to(@tree.has_node_named?('foobar'))
          assert_that false, equal_to(@tree.has_node_named?('bacon'))
        end

        it "can retrieve a node by name" do
          assert_that @node_mock, equal_to(@tree.get_node_named('foobar'))
        end

        it "can give an iterator" do
          iterator = @tree.get_iterator
          assert_that true, equal_to(iterator.is_a?(Hurin::Iterator::RecursiveTreeIterator))
        end

        it "can sort its nodes" do
          new_node = Hurin::Entity::GitObject::Node::BlobNode.new
          new_node.stubs(:name).returns('aardvark')
          @tree.add_node new_node
          assert_that [new_node, @node_mock], equal_to(@tree.nodes)
          first_node = Hurin::Entity::GitObject::Node::BlobNode.new
          first_node.stubs(:name).returns('Zebra')
          @tree.add_node first_node
          assert_that [first_node, new_node, @node_mock], equal_to(@tree.nodes)
        end

        it "can sort nodes one more time" do
          new_node = Hurin::Entity::GitObject::Node::BlobNode.new
          new_node.stubs(:name).returns('1234')
          @tree.add_node new_node
          assert_that [new_node, @node_mock], equal_to(@tree.nodes)
        end
      end
    end
  end
end
