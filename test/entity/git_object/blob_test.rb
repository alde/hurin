require 'testhelper'

module Hurin
  module Entity
    module GitObject
      describe Blob do
        include Ramcrest::EqualTo

        before do
          @blob = Blob.new
        end

        it "can hold a sha hash" do
          @blob.sha = "deadbeef"
          assert_that @blob.sha, equal_to("deadbeef")
        end

        it "can hold content" do
          @blob.content = "content here"
          assert_that @blob.content, equal_to("content here")
        end
      end
    end
  end
end
