require 'testhelper'

module Hurin
  module Entity
    module GitObject

      describe User do
        include Ramcrest::EqualTo

        it "can be created" do
          user = Hurin::Entity::GitObject::User.new "Egg", "bacon@spam.nu"
          assert_that "Egg", equal_to(user.name)
          assert_that "bacon@spam.nu", equal_to(user.email)
        end
      end
    end
  end
end
