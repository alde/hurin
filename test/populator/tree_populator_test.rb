require 'testhelper'

module Hurin
  module Populator

    describe TreePopulator do
      include Ramcrest::EqualTo

      it "can resolve a 'happy' path" do
        sha = "deadbeefcafebabefacebadc0ffeebadf00dcafe"
        blob_line = sprintf(
          "%s %s\0%s",
          "100644",
          'testblob.md',
          ["deadbeefcafebabefacebadc0ffeebadf00dface"].pack("H*")
        )
        tree_line = sprintf(
          "%s %s\0%s",
          "40000",
          'testtree',
          ["deadbeefcafebabefacebadc0ffeebadf00dbeef"].pack("H*")
        )

        data = "#{blob_line}#{tree_line}"
        raw = stub('raw_object')
        raw.stubs(:sha).returns(sha)
        raw.stubs(:data).returns(data)

        repo = stub('repository')

        populator = Hurin::Populator::TreePopulator.new repo

        tree = populator.populate raw

        assert_that sha, equal_to(tree.sha)

        nodes = tree.nodes

        assert_that 'testblob.md', equal_to(nodes[0].name)
        assert_that '100644', equal_to(nodes[0].octal_mode)
        assert_that 'deadbeefcafebabefacebadc0ffeebadf00dface', equal_to(nodes[0].get_related_object.sha)
        assert_that true, equal_to(nodes[0].is_a?(Hurin::Entity::GitObject::Node::BlobNode))

        assert_that 'testtree', equal_to(nodes[1].name)
        assert_that '40000', equal_to(nodes[1].octal_mode)
        assert_that 'deadbeefcafebabefacebadc0ffeebadf00dbeef', equal_to(nodes[1].get_related_object.sha)
        assert_that true, equal_to(nodes[1].is_a?(Hurin::Entity::GitObject::Node::TreeNode))
      end

      it "can resolve a path with a space in the filename" do
        sha = "deadbeefcafebabefacebadc0ffeebadf00dcafe";
        blob_line = sprintf(
          "%s %s\0%s",
          "100644",
          'Test blob.md',
          ["deadbeefcafebabefacebadc0ffeebadf00dface"].pack("H*")
        )

        raw = stub('raw_data')
        raw.stubs(:sha).returns(sha)
        raw.stubs(:data).returns(blob_line)

        populator = Hurin::Populator::TreePopulator.new stub('repository')

        tree = populator.populate raw
        assert_that 'Test blob.md', equal_to(tree.nodes[0].name)
      end
    end
  end
end
