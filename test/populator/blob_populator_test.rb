require 'testhelper'

module Hurin
  module Populator

    describe BlobPopulator do
      include Ramcrest::EqualTo

      it "can resolve a 'happy' path" do
        sha = "deadbeefcafebabefacebadc0ffeebadf00dcafe"
        data = "A blob"

        raw = stub('raw_data')
        raw.stubs(:sha).returns(sha)
        raw.stubs(:data).returns(data)

        populator = Hurin::Populator::BlobPopulator.new

        blob = populator.populate raw

        assert_that sha, equal_to(blob.sha)
        assert_that data, equal_to(blob.content)
      end
    end
  end
end
