require 'testhelper'

module Hurin
  module Populator

    describe CommitPopulator do
      include Ramcrest::EqualTo

      it "can resolve a 'happy' path" do
        sha = "c3c8d149d63338c1215e0b06c839768cdf1933d5"
        data = <<-EOT
tree b4ac469697c1e0f5fbb5702befc59fd7a90970a0
parent b3bf978c063bc6491919a97d1f6d6c9a6c074a2d
parent deadbeefcafec0ffee19a97d1f6d6c9a6c074a2d
parent cafebabe063bc6491919a97d1f6d6c9a6c074a2d
author Rickard Dybeck <rickard@alde.nu> 1316430078 +0200
committer Rickard Dybeck <rickard@alde.nu> 1316430078 +0200

Updated to version 2.1.2
EOT

        raw = stub('raw_object')
        raw.stubs(:sha).returns(sha)
        raw.stubs(:data).returns(data)

        repo = stub('repository')

        populator = Hurin::Populator::CommitPopulator.new repo

        commit = populator.populate raw

        assert_that sha, equal_to(commit.sha)
        assert_that "b4ac469697c1e0f5fbb5702befc59fd7a90970a0", equal_to(commit.tree.sha)
        parents = commit.parents
        assert_that "b3bf978c063bc6491919a97d1f6d6c9a6c074a2d", equal_to(parents[0].sha)

        user = Hurin::Entity::GitObject::User.new 'Rickard Dybeck', 'rickard@alde.nu'
        assert_that user, equal_to(commit.author)
        assert_that user, equal_to(commit.committer)

        date = DateTime.strptime "1316430078 +0200", "%s"
        assert_that date.to_time, equal_to(commit.author_time)
        assert_that date.to_time, equal_to(commit.commit_time)
      end
    end
  end
end
